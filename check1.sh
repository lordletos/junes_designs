#!/bin/bash


file=$1
IFS=$'\n'
if [ -r $file ]
    then
      echo "OK"
    else
      echo "$(date) fail file"
      exit 0;
fi

function check {
    curl -s -o /dev/null -w "%{http_code}" $LINE
    return $code
}


#for var in $(cat $file)
while read LINE
   do
     result=$( check $LINE)
     echo $LINE $result
     if [ $result -gt 400 ]
       then
         echo "$(date) $var $result" >> checkresult.log
         break;
     fi
done < $file

